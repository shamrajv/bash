# Purpose/ Banner : AutoMaps Modification Script to update Maps with Netapp Servers/Volumes
# Version:          1.4
# Author:           vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash
OS=`uname -s`
HOST=`hostname`
BASEDIR=/var/tmp
CLIENT=`hostname |awk -F"." '{print $1}'`
GFSFILE=${BASEDIR}/${CLIENT}_gfsshareinfo

bk_up() { #function to back up files in the same directory as original file
                        cp -p /etc/$1 /var/tmp/$1.bkup.`date '+%d%m%Y_%H_%M_%S'`
                        cp -p /etc/$1 /var/tmp/$1.bkup.pregfsmigration
        }

factormaps_linux() {

maps=`ls /etc | grep -i "auto\."`
mapfile=`for map in $maps; do  if [ -f /etc/$map ] ; then echo $map; fi;  done`
for amap in ${mapfile}
do set `echo ${amap}`
  cat ${BASEDIR}/gfssharelist | while read old new
        do sed -i 's,'"$old"','"$new"',g' /etc/$1
        done
   shift
done
                   }

factormaps_solaris() {

maps=`ls /etc | grep -i "auto_"`
mapfile=`for map in $maps; do  if [ -f /etc/$map ] ; then echo $map; fi; done`
for amap in ${mapfile}
do
  set `echo ${amap}`
  cat ${BASEDIR}/gfssharelist | while read old new
        do sed -i 's,'"$old"','"$new"',g' /etc/$1
        done
  shift
done
                    }

#Main_exec

awk '{ print $2":"$3"\t\t"$4":"$5 }' ${GFSFILE} > ${BASEDIR}/gfssharelist

if      [ ${OS} = "Linux" -o ${OS} = "GNU/Linux" ]
then
    maps=`ls /etc | grep -i "auto\."`
    mapfile=`for map in $maps; do  if [ -f /etc/$map ] ; then echo $map; fi;  done`
    for amap in ${mapfile}
    do
      set `echo ${amap}`
      grep -v "`awk '{print $2":"$3}' ${GFSFILE}`" /etc/$1 > ${BASEDIR}/$1
      bk_up $1
      shift
    done
    factormaps_linux ${GFSFILE}
elif    [ ${OS} = "SunOS" ]
then
    maps=`ls /etc | grep -i "auto_"`
    mapfile=`for map in $maps; do if [ -f /etc/$map ] ; then echo $map; fi;  done`
    for amap in ${mapfile}
    do
      set `echo ${amap}`
      /usr/xpg4/bin/grep -v "`awk '{print $2":"$3}' ${GFSFILE}`" /etc/$1 > ${BASEDIR}/$1
      bk_up $1
      shift
    done
    factormaps_solaris ${GFSFILE}
fi
