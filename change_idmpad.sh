# Purpose/ Banner : Idmap Modification Script to update Maps with Netapp Servers/Volumes
# Version:          2.0
# Author:           vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash
OS=`uname -s`
HOST=`hostname`
BASEDIR=/var/tmp
CLIENT=`hostname |awk -F"." '{print $1}'`
GFSFILE=${BASEDIR}/${CLIENT}_gfsshareinfo
if [ -f /etc/os-release ] ; then
                OSFLAVOR=`grep ^NAME /etc/*release | awk -F"=" '{print $2}' | sed 's/\"//g'`
fi
SLESVERS=`awk -F"=" '/^VERSION=/ {print $2}' /etc/os-release | sed 's/"//g' |awk -F"." '{print $1}'`

bk_up() {
                        cp -p $1 $1.bkup.`date '+%d%m%Y_%H:%M:%S'`
        }

ch_sles() {
                domain=`domainname`
                cp -p /etc/idmapd.conf /etc/idmapd.conf.pregfsmig.primal
                origconf=`grep -i '^Domain' /etc/idmapd.conf`
                if [ "${origconf}" != "" ]; then
                        olddomain=`grep '^Domain' /etc/idmapd.conf | awk -F"=" '{print $2}'`
                        sed 's,'"${olddomain}"','" ${domain}"',g' /etc/idmapd.conf > ${BASEDIR}/idmaptmp
                       cp -p ${BASEDIR}/idmaptmp /etc/idmapd.conf
                else
                        echo "Domain = `echo ${domain}`" >> /etc/idmapd.conf
                fi

                echo "INFO : <IDMAP> : ${CLIENT} : Modified the idmapd.conf"

                                #if [ $SLESVERS -eq 11 ]; then
                                #       if [ -f `which service` ]; then
                                #               echo "NFS client : `service nfs status | grep 'Active'`"
                                #               service nfs restart ; echo $?
                                                #echo "NFS client : `service nfs status | grep 'Active'`"
                                                #echo "NFS-IDMAPD: `service nfs-idmapd status | grep 'Active'`"
                                                #service nfs-idmapd restart > /dev/null ; echo $?
                                                #echo "NFS-IDMAPD: `service nfs-idmapd status | grep 'Active'`"
                                #       elif [ -f `which systemctl` ]; then
                                #               echo "NFS Client : `systemctl status nfs | grep 'Active'`"
                                #               systemctl restart nfs | grep 'Active'
                                #               echo "NFS Client : `systemctl status nfs | grep 'Active'`"
                                                #echo "NFS-IDMAPD: `systemctl status nfs-idmapd | grep 'Active'`"
                                                #systemctl restart nfs-idmapd | grep 'Active'
                                                #echo "NFS-IDMAPD: `systemctl status nfs-idmapd | grep 'Active'`"
                                #       fi
        if [ $SLESVERS -eq 10 ]; then
                echo "INFO: SLES 10 Stopping idmapd"
                /etc/init.d/idmapd stop
                /etc/init.d/idmapd start
                if [ $? -eq 0 ]; then echo "INFO : ${CLIENT} :SLES 10 Started idmapd" ; else echo "ERROR : ${CLIENT} : Check idmapd daemon" ; fi
        fi
                echo "=========================================================="
        }

ch_redhat() {
                domain=`domainname`
                cp -p /etc/idmapd.conf /etc/idmapd.conf.pregfsmig.primal
                origconf=`grep -i '^Domain' /etc/idmapd.conf`
                if [ "${origconf}" != "" ]; then
                        olddomain=`grep '^Domain' /etc/idmapd.conf | awk -F"=" '{print $2}'`
                        sed 's,'"${olddomain}"','"${domain}"',g' /etc/idmapd.conf > ${BASEDIR}/idmaptmp
                        cp -p ${BASEDIR}/idmaptmp /etc/idmapd.conf
                else
                        echo "Domain = `echo ${domain}`" >> /etc/idmapd.conf
                fi
                echo "=========================================================="
                echo "INFO : <IDMAP> : ${CLIENT} : Modified the idmapd.conf"
               systemctl restart nfs-idmapd
                echo "INFO : <IDMAP> : ${CLIENT} : Restarted idmapd daemon"
                systemctl  status nfs-idmapd
                dmesg | grep -i id_resolver
                echo "=========================================================="
        }

ch_sol() {
                cp -p /etc/default/nfs /etc/default/nfs.pregfsmig.primal
                domain=`domainname`
                origconf=`grep -i "^NFSMAPID_DOMAIN="  /etc/default/nfs`
                if [ "${origconf}" != "" ]; then
                        olddomain=`grep '^NFSMAPID_DOMAIN=' /etc/default/nfs | awk -F"=" '{print $2}'`
                        sed 's,'"${olddomain}"','"${domain}"',g' /etc/default/nfs > ${BASEDIR}/nfstmp
                        cp -p ${BASEDIR}/nfstmp /etc/default/nfs
                else
                        echo "NFSMAPID_DOMAIN=`echo ${domain}`" >> /etc/default/nfs
                fi
                        echo "=========================================================="
                        echo "INFO : <IDMAP> : ${CLIENT} : Modified the nfs configs"
                        svcadm restart svc:/network/nfs/mapid:default
                                                svcs -a | grep -i "nfs/mapid"
                        echo "INFO : <IDMAP> : ${CLIENT} : Restarted nfs/mapid"
                        echo "=========================================================="
                }


case ${OS} in
                SunOS)  bk_up /etc/default/nfs
                        ch_sol
                ;;
                Linux)  bk_up /etc/idmapd.conf
                        if [ "${OSFLAVOR}" = "Red Hat Enterprise Linux Server" ];then
                                ch_redhat
                        elif [ "${OSFLAVOR}" = "SLES" ]; then
                                ch_sles
                        fi
                ;;
                *) echo "Works only for OS Types SunOS & Linux {RHEL , SLES}"
                        exit
                ;;
esac
