# Purpose/ Banner :     This script executes in each nfs clients to check new mounts Accessibility.
# Version:              1.0
# Author:               vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash

#Variable Declaration
OS=`uname -s`
HOST=`hostname`
BASEDIR=/var/tmp
CLIENT=`hostname |awk -F"." '{print $1}'`
GFSFILE=${BASEDIR}/${CLIENT}_gfsshareinfo
OLDGFS=`awk '{print $1}' $GFSFILE`
NEWGFS=`awk '{print $3}' $GFSFILE`



bk_up() { #FUNCTION TO BACK UP FILES IN THE SAME DIRECTORY AS ORIGINAL FILE
               cp -p $1 $1.bkup.`date '+%d%m%Y_%H:%M:%S'`
        }


check_mounts_linux()   { # FUNCTION TO CHECK MOUNT NEW NFS SHARES FOR LINUX
                                awk '{
                                h = "hostname"
                                h | getline host
                                close(h)
                                if (system("cd " $6) != 0)
                                print "ERROR: $1 <DIRECTORY ACCESS FAILED> :", $4":"$5, $6
                                #else print "SUCCESS: host <DIRECTORY ACCESS SUCCEEDED> :", $4":"$5, $6
                                #if (system("df -h " $6) != 0)
                                #print "ERROR: host <df MOUNT POINT> :", $4":"$5, $6
                                #else print "SUCCESS: host <df MOUNT POINT> :", $4":"$5, $6
                                }' ${GFSFILE}
                        }

check_mounts_solaris()   { # FUNCTION TO CHECK MOUNT NEW NFS SHARES FOR Solaris
                                /usr/xpg4/bin/awk '{
                                h = "hostname"
                                h | getline host
                                close(h)
                                if (system("cd " $6) != 0)
                                print "ERROR: $1 <DIRECTORY ACCESS FAILED> :", $4":"$5, $6
                                #else print "SUCCESS: host <DIRECTORY ACCESS SUCCEEDED> :", $4":"$5, $6
                                #if (system("df -h " $6) != 0)
                                #print "ERROR: host <df MOUNT POINT> :", $4":"$5, $6
                                #else print "SUCCESS: host <df MOUNT POINT> :", $4":"$5, $6
                                }' ${GFSFILE}
                        }

#####MAIN_EXEC######

if [ ${OS} = "Linux" -o ${OS} = "GNU/Linux" ];then

        check_mounts_linux

elif [ ${OS} = "SunOS" ]; then

        check_mounts_solaris

fi
