#!/bin/bash
OS=`uname -s`

autofs_in_linux() {

for mapdir in `grep /etc/auto /etc/mtab | awk '{print $2}'`
        do  map_dir=`echo $mapdir | sed 's/\///'`
                if [ -f /etc/auto.${map_dir} ]; then
                        awk -v map="$map_dir" '{
                                                 h = "hostname"
                                                 h | getline host
                                                 close(h)
                                                 print host"\t/etc/auto."map"\t"$3"\t"$1
                                               }' /etc/auto.${map_dir}
                fi
        done
                                        }

autofs_in_solaris() {
for mapdir in `grep "auto_" /etc/mnttab | /usr/xpg4/bin/awk '{print $2}'`
        do  map_dir=`echo $mapdir | sed 's/\///'`
                if [ -f /etc/auto_${map_dir} ];  then
                /usr/xpg4/bin/awk -v map="$map_dir" '{
                                        h = "hostname"
                                        h | getline host
                                        close(h)
                                        print host"\t/etc/auto_"map"\t"$0
                                        }' /etc/auto_${map_dir}
                fi
        done
                                        }

if [ ${OS} = "Linux" -o ${OS} = "GNU/Linux" ]
then
        autofs_in_linux | awk 'NF == 4 && $3 !~ "^[\\[\\:]" { sub(/:/," ");print $0}'| egrep -v "#$|ftp.example.org|localhost"
elif [ ${OS} = "SunOS" ]
then
        autofs_in_solaris | /usr/xpg4/bin/awk 'NF == 5 && $3 != "#"  {sub(/:/," "); print $1, $2, $5, $6, $3, $4}'|egrep -v "localhost"
fi
