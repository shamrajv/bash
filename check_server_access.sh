# Purpose/ Banner :     This script executes in sath server, to check the connectivignt using ping /ssh status
#                       logged in notreachable.log / ping.log / ssh.log. Nullify the log files after every use.
# Version:              1.0
# Author:               vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash

serverlist=$1
for servers in `cat $serverlist`
do
echo "*****Checking SSH Connection to ${servers}*****" > /dev/tty
ssh -o 'ConnectTimeout 5' -o 'ConnectionAttempts 1' -o 'BatchMode yes' -o 'StrictHostKeyChecking no' -l root ${servers} 'uname -s'
if [ $? != 0 ]; then echo "$servers not responding to SSH" >> notreachable.log; else echo $servers >> reachable.log; fi
done
