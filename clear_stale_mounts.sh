# Purpose/ Banner :      This script executes in each nfs clients to figure out any stale mounts and clear them
# Version:               1.0
# Author:                vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash

# Variable Declaration
OS=`uname -s`
HOST=`hostname`
BASEDIR=/var/tmp
CLIENT=`hostname |awk -F"." '{print $1}'`
GFSFILE=${BASEDIR}/${CLIENT}_gfsshareinfo

df -h > ${BASEDIR}/dfhstale 2>&1

for dirs in `awk -F":" 'NF == 3 {print $2}' /var/tmp/dfhstale |tr -cd "[:print:]\n" | tr -d \' | sed 's/\`//g'`
do
        echo "Unmounting $dirs"
        if [ ${OS} = "Linux" -o ${OS} = "GNU/Linux" ]
                then umount -l ${dirs} ; echo $?
        elif [ ${OS} = "SunOS" ]
                then umount -f ${dirs} ; echo $?
        fi
done

