# Purpose/ Banner :             This script executes in sath server, and prepares nfs clients by staging the required informational file and
#                               copying the unmount script over to nfs clients.
# Version:                      1.0
# Author:                       vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash
#set -x
# Variable Declaration
BASEDIR=/var/tmp
MIGRATIONFILE=`pwd`/gfs.migration.input
NFSCLIENTS=`pwd`/nfsclients
GFSFILE=`pwd`/${clients}_gfsshareinfo

create_CLIENT_share_info()
                { # Creates the NFSCLIENT's shareinfo and remote copies it to NFSCLIENTS.
                 # Also copies over unmount script

        for clients in `cat ${NFSCLIENTS}`
            do
                awk -v client="${clients}" '$2 ~ client {print $2, $3, $4, $5, $6, $7}'  ${MIGRATIONFILE} > ${clients}_gfsshareinfo
                sed -e 's/:/ /g' ${clients}_gfsshareinfo > ${clients}_gfsshareinfo.tmp
                mv ${clients}_gfsshareinfo.tmp ${clients}_gfsshareinfo
                scp  -o 'ConnectTimeout 5' -o 'ConnectionAttempts 1' -o 'BatchMode yes' -o 'StrictHostKeyChecking no' -p ${clients}_gfsshareinfo root@${clie
nts}:/var/tmp
                if [ $? != 0 ]; then echo "${clients} : FAILED : REMOTE COPY for shareinfo did not succeed">> scpmonitor.log ;fi
            done
                }

if [ -s `pwd`/scpmonitor.log ]; then
mv `pwd`/scpmonitor.log `pwd`/scpmonitor.log.`date '+%d%m%Y_%H_%M_%S'`
fi

create_CLIENT_share_info
