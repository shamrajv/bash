# Purpose/ Banner : This script executes in each nfs clients to mount new shares
#                   from netapp as specified in ${CLIENT}_gfshareinfo file,
#                   prepared for each nfs client.
# Version:          2.0
# Author:           vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash
#set -x
#Variable Declaration
OS=`uname -s`
HOST=`hostname`
BASEDIR=/var/tmp
CLIENT=`hostname |awk -F"." '{print $1}'`
GFSFILE=${BASEDIR}/${CLIENT}_gfsshareinfo
OLDGFS=`awk '{print $2}' $GFSFILE`
NEWGFS=`awk '{print $4}' $GFSFILE`
FSTABFILE=${BASEDIR}/fstab
VFSTABFILE=${BASEDIR}/vfstab
if [ ${OS} = "SunOS" ] ; then  ORIGFSTAB=/etc/vfstab
else ORIGFSTAB=/etc/fstab
fi
#ORIGGFSFILE=${GFSFILE}.bkup.pregfsmigration
if [ -f /etc/os-release ] ; then
        OSFLAVOR=`grep ^NAME /etc/*release | awk -F"=" '{print $2}' | sed 's/\"//g'`
fi


# function to back up files in the
# same directory as original file
bk_up() {
                        cp -p $1 $1.bkup.`date '+%d%m%Y_%H:%M:%S'`
                        cp -p $1 $1.bkup.pregfsmigration
        }

modfstab_Linux ()
{
# 1st awk expression concatenates ${ORIGFSTAB} and ${GFSFILE}
# lines based on matching array index
awk 'NR==FNR {nfs[$1":"$2]=$0;next } $2":"$3":"$6 in nfs {print nfs[ $2":"$3":"$6],$0 }' ${ORIGFSTAB} ${GFSFILE} > ${BASEDIR}/mountlist

if [ -s ${BASEDIR}/mountlist ]; then
                #2nd awk, expression creates the ${ORIGFSTAB} contents
                awk '{  if ($13 != "autofs") print $10":"$11,$12,$3,$4",vers="$14,$5,$6 }'  ${BASEDIR}/mountlist >> ${TMPFSTAB}
                echo "INFO : <MOD[V]FSTAB> : ${CLIENT} : Created Modified /etc/fstab"
                cp -p ${TMPFSTAB} ${ORIGFSTAB}
else
               echo "INFO : <MOD[V]FSTAB> : ${CLIENT} : No Entries for FSTAB"
fi

# 3rd awk, expression, creates the list of NFS mounts not
# specified in ${ORIGFSTAB}, from ${GFSFILE} stores the
# output in  ${GFSFILE}.remainder
awk 'NR==FNR {nfs[$1":"$2];next} !($2":"$3":"$6 in nfs) && $7 != "autofs" {print $0 }' ${ORIGFSTAB} ${GFSFILE} > ${GFSFILE}.remainder

}


modfstab_sles ()
{

# Placing the Target Netapp into Fstab, inline
# Backup already taken
awk '{ print $2":"$3"\t\t"$4":"$5 }' ${GFSFILE} > ${BASEDIR}/gfssharelist

cat ${BASEDIR}/gfssharelist | while read old new
                do sed -i 's,'"$old"','"$new"',g' ${ORIGFSTAB}
                done

# Correct the four field , SUSE Fstab
awk '{ if ( NF == 4 )
                print $1, $2, $3, $4, "0 0"
           else
            print $0
                }' ${ORIGFSTAB} > ${BASEDIR}/fstab.tmp

# Concatenate, ${BASEDIR}/fstab.tmp and ${GFSFILE} now
awk 'NR==FNR {nfs[$1":"$2]=$0;next } $4":"$5":"$6 in nfs {print nfs[ $4":"$5":"$6],$0 }' ${BASEDIR}/fstab.tmp ${GFSFILE} > ${BASEDIR}/mountlist

if [ -s ${BASEDIR}/mountlist ]; then
#2nd awk, expression creates the ${ORIGFSTAB} contents
#using  ${BASEDIR}/mountlist and stores in ${TMPFSTAB}
awk '{  if ($13 != "autofs") print $10":"$11,$12,$3,$4",nfsvers="$14,$5,$6 }'  ${BASEDIR}/mountlist >> ${TMPFSTAB}
echo "INFO : <MOD[V]FSTAB> : ${CLIENT} : Created Modified /etc/fstab"
else
echo "INFO : <MOD[V]FSTAB> : ${CLIENT} : No Entries for FSTAB"
fi

cp -p ${TMPFSTAB} ${ORIGFSTAB}

# 3rd awk, expression, creates the list of NFS mounts not
# specified in ${BASEDIR}/fstab.tmp, from ${GFSFILE} stores the
# output in  ${GFSFILE}.remainder
awk 'NR==FNR {nfs[$1":"$2];next} !($2":"$3":"$6 in nfs) && $7 != "autofs" {print $0 }' ${BASEDIR}/fstab.tmp ${GFSFILE} > ${GFSFILE}.remainder

}

modfstab_Solaris()  {

# 1st awk expression concatenates ${ORIGFSTAB} and
# ${GFSFILE} lines based on matching array index
/usr/xpg4/bin/awk 'NR==FNR { nfs[$1":"$3]=$0;next } $2":"$3":"$6 in nfs { print nfs[ $2":"$3":"$6],$0 }' ${ORIGFSTAB} ${GFSFILE} > ${BASEDIR}/mountlist

if [ -s ${BASEDIR}/mountlist ]; then
        # 2nd awk, expression creates the
        # target ${ORIGFSTAB} contents

        /usr/xpg4/bin/awk '{if ($14 != "autofs") print $11":"$12,"-",$13,"nfs -",$6,$7",vers="$15}'  ${BASEDIR}/mountlist >> ${TMPFSTAB}
        echo "INFO : <MOD[V]FSTAB> : ${CLIENT} : Created Modified /etc/vfstab"
        cp -p ${TMPFSTAB} ${ORIGFSTAB}
else
        echo "INFO : <MOD[V]FSTAB> : ${CLIENT} : No Entries for VFSTAB"
fi

# 3rd awk, expression, creates the list of
# NFS mounts not specified in ${ORIGFSTAB},
# but part of ${GFSFILE} & stores the output
# in ${BASEDIR}/${GFSFILE}.remainder

/usr/xpg4/bin/awk 'NR==FNR {nfs[$1":"$3];next} !($2":"$3":"$6 in nfs) && $7 != "autofs" {print $0 }' ${ORIGFSTAB} ${GFSFILE} > ${GFSFILE}.remainder

                                        }

#####MAIN_EXEC######
bk_up ${GFSFILE}


if [ "${OS}" = "GNU/Linux" -o "${OS}" = "Linux" ]
        then
        bk_up /etc/fstab
        grep -v "`awk '{print $2":"$3}' ${GFSFILE}`" /etc/fstab > ${BASEDIR}/fstab
        TMPFSTAB=${BASEDIR}/fstab
elif [ "${OS}" = "SunOS" ]
        then
                bk_up /etc/vfstab
                /usr/xpg4/bin/grep -v "`awk '{print $2":"$3}' ${GFSFILE}`" /etc/vfstab > ${BASEDIR}/vfstab
                TMPFSTAB=${BASEDIR}/vfstab      #${TMPFSTAB} holds [v]fstab without nfs entries
fi

if [ "${OS}" = "Linux" -a "${OSFLAVOR}" = "Red Hat Enterprise Linux Server" ]
        then
                 modfstab_Linux ${TMPFSTAB} ${OS}
elif [ "${OS}" = "SunOS" ]
        then
                 modfstab_Solaris ${TMPFSTAB} ${OS}
elif [ "${OS}" = "Linux" -a "${OSFLAVOR}" = "SLES" ]
                then
                                 modfstab_sles ${TMPFSTAB} ${ORIGFSTAB}
fi
