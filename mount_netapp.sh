# Purpose/ Banner :     This script executes in each nfs clients to mount new shares
#                       from netapp as specified in ${CLIENT}_gfshareinfo file,
#                       prepared for each nfs client.
# Version:              1.0
# Author:               vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash

#Variable Declaration
OS=`uname -s`
HOST=`hostname`
BASEDIR=/var/tmp
CLIENT=`hostname |awk -F"." '{print $1}'`
GFSFILE=${BASEDIR}/${CLIENT}_gfsshareinfo
OLDGFS=`awk '{print $1}' $GFSFILE`
NEWGFS=`awk '{print $3}' $GFSFILE`



bk_up() { #FUNCTION TO BACK UP FILES IN THE SAME DIRECTORY AS ORIGINAL FILE
               cp -p $1 $1.bkup.`date '+%d%m%Y_%H:%M:%S'`
        }


mount_new_Linux_gfs()   { # FUNCTION TO MOUNT NEW NFS SHARES FOR LINUX
                                awk '{
                                        h = "hostname"
                                        h | getline host
                                        close(h)
                                        if($13 != "autofs" )
                                        if (system("mount " $12) != 0)
                                        print "ERROR: <Mount> :", $7, $10":"$11, $12
                                        else print "SUCCESS: <Mount> :", $7, $10":"$11, $12
                                        }' /var/tmp/mountlist.tmp
                        }

mount_new_Solaris_gfs() { # FUNCTION TO MOUNT NEW NFS SHARES FOR SOLARIS
                                /usr/xpg4/bin/awk '{
                                               h = "hostname"
                                               h | getline host
                                               close(h)
                                                if($14 != "autofs" )
                                                if (system("mount " $13) != 0)
                                                print "ERROR: <Mount> :", $8, $11":"$12, $13
                                                else print "SUCCESS: <Mount> :", $8, $11":"$12, $13
                                        }' /var/tmp/mountlist.tmp
                        }


#####MAIN_EXEC######

if [ ${OS} = "Linux" -o ${OS} = "GNU/Linux" ];then
                                grep -v autofs /var/tmp/mountlist > /var/tmp/mountlist.tmp
                 mount_new_Linux_gfs
                                 mount -v | grep -i nfs > ${BASEDIR}/mount.postgfsmigration
elif [ ${OS} = "SunOS" ]; then
                                grep -v autofs /var/tmp/mountlist > /var/tmp/mountlist.tmp
                 mount_new_Solaris_gfs
                                 mount -v | grep -i nfs > ${BASEDIR}/mount.postgfsmigration
fi
