# Purpose/ Banner :      This script executes in each nfs clients to unmount the gfsshare
#                        as mentioned in ${CLIENT}_gfsshareinfo file, prepared for each nfs client.
# Version:               1.0
# Author:                vivek.shamraj@dxc.com itoindiagdc.dbtransunix@dxc.com
#!/bin/bash

# Variable Declaration
OS=`uname -s`
HOST=`hostname`
BASEDIR=/var/tmp
CLIENT=`hostname |awk -F"." '{print $1}'`
GFSFILE=${BASEDIR}/${CLIENT}_gfsshareinfo
if [ ! -d ${BASEDIR} ]; then mkdir -p ${BASEDIR} ; fi


bk_up() {  # Backs up argumentary file in its own source directory location and in ${BASEDIR}
           cp -p $1 $1.bkup.`date '+%d%m%Y_%H:%M:%S:%s'`
                   cp -p $1 $1.bkup.pregfsmigration
        }


decide_Linux_umount() { # Function to isolate the right NFS share to un-mount,  then call remove_old_gfs_mount function
                        mount -v | awk '$5 == "nfs" || $5 == "nfs4" {print $1"\t"$3}' > /var/tmp/nfsmounts
                        sed -e 's/:/ /g' /var/tmp/nfsmounts > /var/tmp/nfsmounts.tmp
                        awk ' NR==FNR{nfs[$1":"$2":"$3]=$3;next} nfs[$2":"$3":"$6] == $6  {print $0} '  /var/tmp/nfsmounts.tmp ${GFSFILE} > /var/tmp/verifie
d.umount.gfsshareinfo
                        if [ -s /var/tmp/verified.umount.gfsshareinfo ] ; then
                                        remove_old_gfs_mount_Linux
                        else
                                        echo "INFO : <UMOUNT> : ${CLIENT} : No Umounts Performed"
                        fi
                      }

remove_old_gfs_mount_Linux() { # Unmounts the old gfs share as per theinformation gathered from verified.umount.gfsshareinfo

                                awk '{
                                        h = "hostname"
                                        h | getline host
                                        close(h)
                                        if (system ("umount -l " $6) == 0 )
                                        print "SUCCESS: <Umount> ", $1, $2":"$3, $6
                                        else
                                        print "ERROR: <Umount> ", $1, $2":"$3, $6
                                        }' /var/tmp/verified.umount.gfsshareinfo

                            }


decide_Solaris_umount() {   # Function to isolate the right NFS shar to un-mount,  then call remove_old_gfs_mount function
                            mount -v | /usr/xpg4/bin/awk '$5 == "nfs" || $5 == "nfs4" {print $1"\t"$3}' > /var/tmp/nfsmounts
                            sed -e 's/:/ /g' /var/tmp/nfsmounts > /var/tmp/nfsmounts.tmp
                            /usr/xpg4/bin/awk ' NR==FNR{a[$1":"$2":"$3]=$3;next} a[$2":"$3":"$6] == $6  {print $0}' /var/tmp/nfsmounts.tmp ${GFSFILE} > /var
/tmp/verified.umount.gfsshareinfo
                            if [ -s /var/tmp/verified.umount.gfsshareinfo ] ; then
                                 remove_old_gfs_mount_Solaris
                            else
                                 echo "INFO : <UMOUNT> : ${CLIENT} : No Umounts performed"
                            fi
                        }


remove_old_gfs_mount_Solaris() { # Unmounts the old gfs share as per the information gathered from verified.umount.gfsshareinfo

                                /usr/xpg4/bin/awk '{
                                                h = "hostname"
                                                h | getline host
                                                close(h)
                                                if (system ("umount -f " $6) == 0 )
                                                print "SUCCESS: <Umount> ", $1, $2":"$3, $6
                                                else
                                                print "ERROR: <Umount> ", $1, $2":"$3, $6
                                                }'  /var/tmp/verified.umount.gfsshareinfo
                               }

#####MAIN_EXEC######
mount -v | grep -i nfs > /var/tmp/mounts.pregfsmigration
bk_up ${GFSFILE}


if [ ${OS} = "Linux" -o ${OS} = "GNU/Linux" ]; then decide_Linux_umount
elif [ ${OS} = "SunOS" ]; then decide_Solaris_umount
fi
